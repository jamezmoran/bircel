#include "Controller.h"

#include <QMdiArea>
#include <QRegExp>
#include "ui_MainWindow.h"
#include "ui_ChannelWindow.h"
#include "ui_SessionWindow.h"

#include <iostream>
using namespace std;

Controller::Controller():
  m_mainWindow(new QMainWindow()),
  m_default_nick("HircClient")
{
}

void Controller::startProgram(){
  Ui::MainWindow mw;
  mw.setupUi(m_mainWindow.get());
  m_mainWindow->show();

  std::shared_ptr<IrcSession> session(new IrcSession());
  QObject::connect(session.get(), SIGNAL(joinedChannel(const QString&)),
                   this, SLOT(createChannel(const QString&)));
  createSessionWindow(session.get());
  m_sessions[session->getSessionId()] = session;

  session->connect("se.quakenet.org",6667,"coldbuttma","butts", "", "buttlord");
}

void Controller::updateSessionText(const QString &origin, const QString &message){
  IrcSession *signal_sender = dynamic_cast<IrcSession*>(sender());
  int sessionId = signal_sender->getSessionId();

  QWidget *channelWindow = m_mainWindow->findChild<QWidget*>(QString("session_")+sessionId);
  if (!channelWindow)
    return;

  QPlainTextEdit *ple = channelWindow->findChild<QPlainTextEdit*>("SessionBox");
  ple->appendPlainText("-"+origin+"- "+message);
}

void Controller::updateChannelChat(int sessionId, const QString &channel, const QString &nickname, const QString &message){
  QMdiArea *mdi = m_mainWindow->findChild<QMdiArea*>("ChannelArea");
  QWidget *channelWindow = mdi->findChild<QWidget*>(QString("channel_")+sessionId+"_"+channel);
  if (!channelWindow)
    return;

  QPlainTextEdit *ple = channelWindow->findChild<QPlainTextEdit*>("ChatBox");
  ple->appendPlainText("<"+nickname+"> "+message);
}

void Controller::updateChannelTopic(int sessionId, const QString &channel, const QString &topic){
  cout << "Controller::updateChannelTopic()" << endl;
  QMdiArea *mdi = m_mainWindow->findChild<QMdiArea*>("ChannelArea");
  QWidget *channelWindow = mdi->findChild<QWidget*>(QString("channel_")+sessionId+"_"+channel);
  if (!channelWindow)
    return;
  channelWindow->setWindowTitle(channel + " (" + m_sessions[sessionId]->getNickname() + "): " + topic);

  QPlainTextEdit *ple = channelWindow->findChild<QPlainTextEdit*>("ChatBox");
  ple->appendPlainText("* Topic is '"+topic+"'");
}
void Controller::updateChannelNames(int sessionId, const QString &channel, const QStringList &names){
  cout << "Controller::updateChannelTopic()" << endl;
  QMdiArea *mdi = m_mainWindow->findChild<QMdiArea*>("ChannelArea");
  QWidget *channelWindow = mdi->findChild<QWidget*>(QString("channel_")+sessionId+"_"+channel);
  if (!channelWindow)
    return;
  
  QListWidget *nlw = channelWindow->findChild<QListWidget*>("NamesBox");
  nlw->clear();
  nlw->insertItems(0, names);
}

void Controller::closeChannel(const int sessionId, const QString& channel){
  m_sessions[sessionId]->partChannel(channel);
}


void Controller::createChannel(const QString &channel){
  IrcSession *signal_sender = dynamic_cast<IrcSession*>(sender());
  int sessionId = signal_sender->getSessionId();


  QMdiArea *mdi = m_mainWindow->findChild<QMdiArea*>("ChannelArea");
  if (!mdi){
    cout << "could not find mdi" << endl;
    return;
  }
  ChannelSubWindow *channelWindow = new ChannelSubWindow(mdi);
  QWidget *channelWidget = new QWidget(channelWindow);
  Ui::ChannelWindow cw_ui;
  cw_ui.setupUi(channelWidget);

  channelWindow->setWidget(channelWidget);
  channelWindow->setObjectName(QString("channel_")+sessionId+"_"+channel);
  channelWindow->setProperty("ChannelName", channel);
  channelWindow->setProperty("SessionId", sessionId);

  QList<QWidget*> children = channelWindow->findChildren<QWidget*>(QRegExp(".*"));
  foreach (QWidget *child, children){
    child->setProperty("ChannelName", channel);
    child->setProperty("SessionId", sessionId);
  }

  mdi->addSubWindow(channelWindow);

  QObject::connect(signal_sender, SIGNAL(receivedChannelMessage(int, const QString&, const QString&, const QString&)),
                   this, SLOT(updateChannelChat(int, const QString&, const QString&, const QString&)));  
  QObject::connect(signal_sender, SIGNAL(topicChanged(int, const QString&, const QString&)),
                   this, SLOT(updateChannelTopic(int, const QString&, const QString&)));  
  QObject::connect(signal_sender, SIGNAL(namesChanged(int, const QString&, const QStringList&)),
                   this, SLOT(updateChannelNames(int, const QString&, const QStringList&)));  
  QObject::connect(channelWindow, SIGNAL(closingChannel(const int, const QString)),
                   this, SLOT(closeChannel(const int, const QString&)),
                   Qt::DirectConnection);
  QObject::connect(channelWindow->findChild<QWidget*>("EditBox"), SIGNAL(returnPressed()),
                   this, SLOT(handleChannelInput()));

  channelWindow->show();
}

void Controller::createSession(const QString& hostname, int port, const QString& nick, const QString& username, const QString& password, const QString& realname){
  std::shared_ptr<IrcSession> session(new IrcSession());
  QObject::connect(session.get(), SIGNAL(joinedChannel(const QString&)),
      this, SLOT(createChannel(const QString&)));
  createSessionWindow(session.get());
  m_sessions[session->getSessionId()] = session;

  session->connect(hostname, port, nick, username, password, realname);

}

void Controller::createSessionWindow(){
  IrcSession *signal_sender = dynamic_cast<IrcSession*>(sender());
  createSessionWindow(signal_sender);
}

void Controller::createSessionWindow(const IrcSession *session){
  int sessionId = session->getSessionId();
  QMdiArea *mdi = m_mainWindow->findChild<QMdiArea*>("ChannelArea");
  QWidget *sessionWindow = new QWidget(mdi);
  sessionWindow->setObjectName(QString("session_")+sessionId);
  Ui::SessionWindow sw_ui;
  sw_ui.setupUi(sessionWindow);

  QObject::connect(session, SIGNAL(receivedNotice(const QString&, const QString&)),
                   this, SLOT(updateSessionText(const QString&, const QString&)));  
  QObject::connect(sessionWindow->findChild<QWidget*>("EditBox"), SIGNAL(returnPressed()),
                   this, SLOT(handleSessionInput()));
  mdi->addSubWindow(sessionWindow);
  sessionWindow->show();
}


void Controller::handleSessionInput(){
  QLineEdit *origin = dynamic_cast<QLineEdit*>(sender());
  QString input = origin->text();
  int sessionId = origin->property("SessionId").toInt();
  origin->clear();
  
  if (QRegExp("/.*").exactMatch(input)){
    CommandParser::UserCommand command = CommandParser::parseCommand(input);
    handleUserCommand(sessionId, command);
  }
    
}

void Controller::handleChannelInput(){
  QLineEdit *origin = dynamic_cast<QLineEdit*>(sender());
  QString input = origin->text();
  QString channel = origin->property("ChannelName").toString();
  int sessionId = origin->property("SessionId").toInt();
  origin->clear();

  if (QRegExp("/.*").exactMatch(input)){
    CommandParser::UserCommand command = CommandParser::parseCommand(input);
    handleUserCommand(sessionId, command, channel);
  } else {
    m_sessions[sessionId]->sendMessage(channel, input);
    updateChannelChat(sessionId, channel, m_sessions[sessionId]->getNickname(), input);
  }
}

void Controller::handleUserCommand(int sessionId, const CommandParser::UserCommand& userCommand, const QString& channel){
  QVector<QString>& params = *userCommand.params;

  printf("command: %i\n", userCommand.type);
  foreach (const QString &param, params){
    printf("param: %s\n", param.toUtf8().data());
  }

  switch (userCommand.type){
    case CommandParser::UserCommand::JOIN:
      m_sessions[sessionId]->joinChannel(params[0]);
      break;
    case CommandParser::UserCommand::PART:
      if (!channel.isEmpty())
        m_sessions[sessionId]->partChannel(params[0]);
      break;
    case CommandParser::UserCommand::KICK:
      break;
    case CommandParser::UserCommand::SERVER:
      {
        QString hostname;
        int port = 6667;

        if (params.size() > 1){
          hostname = params[0];
        } 
        if (params.size() > 2){
          port = atoi(params[1].toUtf8().data());
        }
        /*
           for(int i = 0; i < params.length(); ++i){
           if (params[i] == "-m" && i != params.length()){
           } else {
           }
           }
         */

        createSession(hostname, port, m_default_nick);

        break;
      }
    case CommandParser::UserCommand::UNKNOWN:
    default:
      break;
  }
}
