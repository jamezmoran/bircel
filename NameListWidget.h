#ifndef NAME_LIST_WIDGET_H
#define NAME_LIST_WIDGET_H

#include <QListWidget>

class NameListWidget : public QListWidget
{
  public:
    NameListWidget(QWidget *parent);
};

#endif
