#ifndef IRCSESSION_H
#define IRCSESSION_H

#include <memory>
#include <QObject>
#include <QThread>
#include <QMap>

#include <libircclient/libircclient.h>
#include <libircclient/libirc_rfcnumeric.h>

class IrcSession : public QObject {
  Q_OBJECT
  public:
    IrcSession();
    ~IrcSession();

    enum IrcSessionCode {
      CONNECT_OK,
      CONNECT_FAILED
    };
    
    IrcSessionCode connect(const QString &address, int port, const QString &nick, const QString &username, const QString &password, const QString &realname);
    bool isConnected() const;

    inline int getSessionId() const { return m_sessionId; }
    inline QString getTopic(const QString &channel){ return topics[channel]; }
    inline QString getNickname() const { return m_nickname; }

    void event_connect(const QString &event, const QString &origin, const QStringList &params);
    void event_notice(const QString &event, const QString &origin, const QStringList &params);
    void event_channel(const QString &event, const QString &origin, const QStringList &params);
    void event_join(const QString &event, const QString &origin, const QStringList &params);
    void event_part(const QString &event, const QString &origin, const QStringList &params);
    void event_numeric(unsigned int event, const QString &origin, const QStringList &params);
    void event_topic(const QString &event, const QString &origin, const QStringList &params);

  public slots:
    void joinChannel(const QString &channel, const QString &password="");
    void partChannel(const QString &channel);
    void sendMessage(const QString &dest, const QString &message);
    void changeNick(const QString &nick);
  
  signals:
    void joinedChannel(const QString &channel);
    void topicChanged(int sessionId, const QString &channel, const QString &topic);
    void receivedNotice(const QString &origin, const QString &notice);
    void receivedChannelMessage(int sessionId, const QString &channel, const QString &nickname, const QString &message);
    void namesChanged(int sessionId, const QString &channel, const QStringList &names);

  private:
    const int m_sessionId;
    QString m_nickname;
    QString m_networkName;

    QMap<QString,QString> topics;

    std::shared_ptr<irc_session_t> m_session;
    std::shared_ptr<irc_callbacks_t> callbacks;

    class Connection : public QThread {
      public:
        Connection(const IrcSession *parent) : m_parent(parent) {}

      private:
        void run() {
          if (irc_run(m_parent->m_session.get())){
          }
        }
        const IrcSession *m_parent;
    };
    Connection connection;
};

#endif
